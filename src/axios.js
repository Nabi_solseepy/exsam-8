import  axios from  'axios'

const instance = axios.create({
    baseURL: 'https://burger-project-solseepy.firebaseio.com/'
});

export default instance;