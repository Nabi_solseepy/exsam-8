import React from 'react';
import './Header.css'
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const Header = () => {
    return (
        <div>
            <Navbar color="primary" light expand="md">
                <NavbarBrand href="/">Quotes Central</NavbarBrand>
                <NavbarToggler  />
                <Collapse  navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink  tag={RouterNavLink} to="/">Quotes</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink  tag={RouterNavLink} to="/add">Submit new quote</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>

    );
};

export default Header;