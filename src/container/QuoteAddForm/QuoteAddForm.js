import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {COTIGORIS} from "../../COTIGORIS.";

class QuoteAddForm extends Component {
    state = {
      author: '',
      qoute: ''
    };

    ChangeValue =(event) => {
       const id = event.target.id;
       this.setState({[id]: event.target.value })
            };

    submitHandler  = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state})
    };



    render() {
        return (
            <div>
                <Form className="ProductForm" mrt={10} onSubmit={this.submitHandler}>
                    <h1>Edit a quote</h1>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>Category</Label>
                        <Col sm={10}>
                            <Input type="select" name="category" id="cotegory"
                            >
                                {Object.keys(COTIGORIS).map(categoryId => (
                                <option key={categoryId} value={categoryId}>{COTIGORIS[categoryId]}</option>
                                ))}

                            </Input>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>Author</Label>
                        <Col sm={10}>
                            <Input type="text" name="name" id="author"
                                value={this.state.author} onChange={this.ChangeValue}
                            />
                        </Col>
                    </FormGroup>


                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>Quote text</Label>
                        <Col sm={10}>
                            <Input type="textarea" name="description"
                               id="qoute"
                                   value={this.state.qoute} onChange={this.ChangeValue}

                            />
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <Col sm={{size: 10, offset: 2}}>
                            <Button type="submit">Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default QuoteAddForm;