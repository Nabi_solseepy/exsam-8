import React, {Component} from 'react';
import {Button, Card, CardText, CardTitle, Nav, NavItem, NavLink} from "reactstrap";
import {NavLink as ROUTERNavLink} from "react-router-dom";
import {COTIGORIS} from "../../COTIGORIS.";
import axios from "../../axios";
import Container from "reactstrap/es/Container";
import Col from "reactstrap/es/Col";
import CardColumns from "reactstrap/es/CardColumns";

class NavLinkQuotes extends Component {
    state = {
        quotes: null
    };

    componentDidMount() {
        let url = "/quotes.json";

        axios.get(url).then(result => {
             if(result.data){

                 const quotes = Object.keys(result.data).map(quoteId => {
                     return {...result.data[quoteId], quoteId }
                 });
                 this.setState({quotes})

             }
         })
    }


    deleteHandler = () => {
        axios.delete('/quotes.json').then(() => {
            let url = "/quotes.json";

            axios.get(url).then(result => {
                if(result.data){

                    const quotes = Object.keys(result.data).map(quoteId => {
                        return {...result.data[quoteId], quoteId }
                    });
                    this.setState({quotes})

                } else this.setState({quotes : null})
            });
            this.props.history.push('/')
        })
    };

    render() {
        let quotes = null;
        if (this.state.quotes){
            console.log(this.state.quotes);
            quotes = this.state.quotes.map(quotes => (

                    <Card body inverse color="info">
                        <CardTitle>author: {quotes.author}</CardTitle>
                        <CardText>quotes: {quotes.qoute}</CardText>
                        <Button color="secondary">edit</Button>
                        <Button  onClick={this.deleteHandler}>X</Button>
                    </Card>


            ))
        }




        return (
            <Container>
                <Nav  vertical>
                    {Object.keys(COTIGORIS).map(quates => (
                        <NavItem key={quates}>
                            <NavLink tag={ROUTERNavLink} to={"/quates/" + quates}>
                                {COTIGORIS[quates]}</NavLink>
                        </NavItem>
                    ))}

                </Nav>
                <Col>
                    <CardColumns>
                {quotes}
                    </CardColumns>
                </Col>
            </Container>



        );
    }
}

export default NavLinkQuotes;