import React, {Component, Fragment} from 'react';
import './App.css';
import Header from "./component/Header/Header";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import NavLinkQuotes from "./container/NavLinkQuotes/NavLinkQuotes";
import AddQuotes from "./container/AddQuotes/AddQuotes";


class App extends Component {

  render() {
    return (
        <BrowserRouter>
             <Fragment>

                 <Header/>
                 <Switch>
                     <Route path="/" exact component={NavLinkQuotes}/>
                     <Route path="/add" component={AddQuotes}/>
                     {/*<Route path="/" component={}/>*/}
                     <Route path="/quotes/:oueteId" component={NavLinkQuotes}/>
                     <NavLinkQuotes/>
                 </Switch>
             </Fragment>

        </BrowserRouter>
    );
  }
}

export default App;
